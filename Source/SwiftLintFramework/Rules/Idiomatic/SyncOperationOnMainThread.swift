//
//  SyncOperationOnMainThreadRule.swift
//  swiftlint
//
//  Created by m on 13/05/2019.
//  Copyright © 2019 Realm. All rights reserved.
//

import Foundation
import SourceKittenFramework

extension String {
    subscript(_ range: NSRange) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        let subString = self[start..<end]
        return String(subString)
    }
}

public struct SyncOperationOnMainThreadRule: ConfigurationProviderRule, AutomaticTestableRule {
    public var configuration = SeverityConfiguration(.warning)
    public init() {}
    public static let description = RuleDescription(
        identifier: "sync_on_main_thread",
        name: "Sync on main thread",
        description: "This operation will cause deadlock.",
        kind: .idiomatic,
        nonTriggeringExamples: [
            "DispatchQueue.main.async"
        ],
        triggeringExamples: [
            "DispatchQueue.main.sync"
        ]
        //        corrections: [
        //            "DispatchQueue.main.sync": "DispatchQueue.main.async"
        //        ]
    )
    
    private static let pattern = "(.main.sync)|(DispatchQueue.main.sync)"
    private static let varDeclarationPattern = "(let|var)\\s+[a-zA-Z_][a-zA-Z0-9_]*\\s+=\\s+DispatchQueue.main"
    private static let varDeclarationRegularExpression = regex(varDeclarationPattern)
    
    public func validate(file: File) -> [StyleViolation] {
        
        let contents = file.contents
        let nsstring = contents.bridge()
        let range = NSRange(location: 0, length: nsstring.length)
        
        let varDeclarationRanges = SyncOperationOnMainThreadRule.varDeclarationRegularExpression
            .matches(in: contents, options: [], range: range)
            .compactMap { match -> NSRange? in
                return match.range
        }
        print(varDeclarationRanges.count)
        var varSyncPattern = ""
        for range in varDeclarationRanges {
            let declarationString = contents.substring(from: range.location, length: range.length)
            print(declarationString)
            let variableNameDeclaration = declarationString.split(separator: "=")[0]
            let variableName = variableNameDeclaration.split(separator: " ")[1]
            print(variableName)
            varSyncPattern = "\(variableName).sync"
        }
        return file.match(pattern: SyncOperationOnMainThreadRule.pattern).map {
            StyleViolation(ruleDescription: type(of: self).description,
                           severity: configuration.severity,
                           location: Location(file: file, characterOffset: $0.0.location))
            } + file.match(pattern: varSyncPattern).map {
                StyleViolation(ruleDescription: type(of: self).description,
                               severity: configuration.severity,
                               location: Location(file: file, characterOffset: $0.0.location))
        }
    }
    
}
