//
//  NSCodingRule.swift
//  swiftlint
//
//  Created by m on 15/05/2019.
//  Copyright © 2019 Realm. All rights reserved.
//

import Foundation
import SourceKittenFramework


public struct SecureCodingRule: ConfigurationProviderRule, AutomaticTestableRule {

    public var configuration = SeverityConfiguration(.warning)
    public init() {}
    public static let description = RuleDescription(
        identifier: "use_of_nscoding",
        name: "Use of NSCoding",
        description: "NSCoding is not secure, use NSSecureCoding instead!",
        kind: .idiomatic,
        nonTriggeringExamples: [
            "class Foo: NSObject, NSSecureCoding",
            "extension Foo: NSSecureCoding"
        ],
        triggeringExamples: [
            "class Foo: NSObject, NSCoding",
            "extension Foo: NSCoding"
        ]
    )

    private static let pattern = "NSCoding"

    public func validate(file: File) -> [StyleViolation] {

//        let contents = file.contents
//        let nsstring = contents.bridge()
//        let range = NSRange(location: 0, length: nsstring.length)

        return file.match(pattern: SecureCodingRule.pattern).map {
            StyleViolation(ruleDescription: type(of: self).description,
                           severity: configuration.severity,
                           location: Location(file: file, characterOffset: $0.0.location))
        }
    }

}
